const deepEqual = require("./deepEqual");

describe("deepEqual", () => {
    let obj = { here: { is: "an" }, object: 2 };

    test("compare same object", () => {
        expect(deepEqual(obj, obj)).toBe(true);
    })

    test('compare different objects', () => {
        expect(deepEqual(obj, { here: 1, object: 2 })).toBe(false);
    });

    test("compare different objects with same properties", () => {
        expect(deepEqual(obj, { here: { is: "an" }, object: 2 })).toBe(true);
    })
})

